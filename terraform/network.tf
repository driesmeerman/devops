# Create vpc
resource "aws_vpc" "devops_vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = true #gives you an internal domain name
  enable_dns_hostnames = true
  tags = {
    ManagedBy = "terraform"
    DevOps    = "vpc"
  }
}

# Define subnets
resource "aws_subnet" "subnet-private-1a" {
  vpc_id                  = aws_vpc.devops_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    ManagedBy = "terraform"
    DevOps    = "prod-subnet-private-1a"
  }
}

resource "aws_subnet" "subnet-private-1b" {
  vpc_id            = aws_vpc.devops_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "eu-west-1b"

  tags = {
    ManagedBy = "terraform"
    DevOps    = "prod-subnet-private-1b"
  }
}

# Gateway for public access TMP
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.devops_vpc.id

  tags = {
    ManagedBy = "terraform"
    DevOps    = "aws_internet_gateway"
  }
}

# Route table for public access TMP
resource "aws_route_table" "prod-public-crt" {
  vpc_id = aws_vpc.devops_vpc.id

  route {
    //associated subnet can reach everywhere
    cidr_block = "0.0.0.0/0"
    //CRT uses this IGW to reach internet
    gateway_id = aws_internet_gateway.default.id
  }

  tags = {
    ManagedBy = "terraform"
    DevOps    = "aws_route_table"
  }
}

# connect subnet and route table TMP
resource "aws_route_table_association" "prod-crta-public-subnet-1" {
  subnet_id      = aws_subnet.subnet-private-1a.id
  route_table_id = aws_route_table.prod-public-crt.id
}

# Create security group
resource "aws_security_group" "default" {
  vpc_id = aws_vpc.devops_vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    ManagedBy = "terraform"
    DevOps    = "nsg"
  }
}

# Network interface
resource "aws_network_interface" "nic1" {
  subnet_id       = aws_subnet.subnet-private-1a.id
  security_groups = [aws_security_group.default.id]
}

resource "aws_network_interface" "nic2" {
  subnet_id       = aws_subnet.subnet-private-1b.id
  security_groups = [aws_security_group.default.id]
}

resource "aws_db_subnet_group" "default" {
  name       = "aws_db_subnet_group_devops"
  subnet_ids = [aws_subnet.subnet-private-1a.id, aws_subnet.subnet-private-1b.id]

  tags = {
    ManagedBy = "terraform"
    DevOps    = "aws_db_subnet_group"
  }
}




