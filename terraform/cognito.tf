resource "aws_cognito_user_pool" "devops10" {
  # This is choosen when creating a user pool in the console
  name = "devops10"

  username_attributes = ["email"]

  # POLICY
  password_policy {
    minimum_length                   = "8"
    require_lowercase                = false
    require_numbers                  = false
    require_symbols                  = false
    require_uppercase                = false
    temporary_password_validity_days = 30
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }

  # MFA & VERIFICATIONS
  mfa_configuration = "OFF"

  auto_verified_attributes = ["email"]

  # MESSAGE CUSTOMIZATIONS
  verification_message_template {
    default_email_option  = "CONFIRM_WITH_LINK"
    email_message_by_link = "By signing up, you will have access to the environmental awareness website. Click here to verify your account. {##Click Here##}"
    email_subject_by_link = "Welcome to our website!"
  }

  tags = {
    ManagedBy = "terraform"
    DevOps    = "cognito"
  }

  # DEVICES
  device_configuration {
    challenge_required_on_new_device      = true
    device_only_remembered_on_user_prompt = true
  }
}

resource "aws_cognito_user_pool_domain" "default_domain" {
  user_pool_id = aws_cognito_user_pool.devops10.id
  # DOMAIN PREFIX
  domain = "devops-team-10-uva-2021"
}

resource "aws_cognito_user_pool_client" "air_quality_app" {
  user_pool_id = aws_cognito_user_pool.devops10.id

  # APP CLIENTS
  name                                 = "devops-team-10-client"
  refresh_token_validity               = 30
  read_attributes                      = ["email"]
  write_attributes                     = ["email"]
  allowed_oauth_flows                  = ["implicit", "code"]
  allowed_oauth_scopes                 = ["email", "aws.cognito.signin.user.admin", "openid", "profile"]
  allowed_oauth_flows_user_pool_client = true
  explicit_auth_flows                  = ["ALLOW_CUSTOM_AUTH", "ALLOW_USER_PASSWORD_AUTH", "ALLOW_USER_SRP_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_ADMIN_USER_PASSWORD_AUTH"]
  prevent_user_existence_errors        = "ENABLED"



  # APP INTEGRATION -
  # APP CLIENT SETTINGS
  supported_identity_providers = ["COGNITO"]
  callback_urls                = ["https://${aws_cloudfront_distribution.s3_distribution.domain_name}/login"]
  logout_urls                  = ["https://${aws_cloudfront_distribution.s3_distribution.domain_name}"]

}
