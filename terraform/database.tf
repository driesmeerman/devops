# Configuration for the RDS Database
# [documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance)
# [extra info](https://faraday.ai/blog/how-to-create-an-rds-instance-with-terraform/)
resource "aws_db_instance" "air_quality_database" {
  name = "air_quality_database"
  # allocated and max storage is 10 GB:
  allocated_storage = 10
  engine            = "mysql"

  # type of EC2 instance to use for RDS:
  # <https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html>
  instance_class = "db.t2.micro"

  db_subnet_group_name   = aws_db_subnet_group.default.id
  vpc_security_group_ids = [aws_security_group.default.id]

  # master username and password (see gitlab):
  username = var.RDS_DATABASE_USER
  password = var.RDS_DATABASE_PASSWORD

  skip_final_snapshot     = true
  backup_retention_period = 0
  apply_immediately       = true
  publicly_accessible     = true

  tags = {
    Database  = "air_quality_database"
    ManagedBy = "terraform"
    DevOps    = "database"
  }

  provisioner "local-exec" {
    command = "mysql --host=${self.address} --port=${self.port} --user=${self.username} --password=${self.password} < ../database/init.sql"
  }

}


