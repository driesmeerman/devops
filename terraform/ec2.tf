resource "aws_instance" "ec2_backend" {
  ami           = "ami-06fd78dc2f0b69910"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.nic1.id
    device_index         = 0
  }

  key_name = "ec2-ssh-key"

  tags = {
    ManagedBy = "terraform"
    DevOps    = "ec2-instance"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir python-backend"
    ]
  }

  # file to turn it into a service
  provisioner "file" {
    source      = "../python-backend/flask.service"
    destination = "~/python-backend/flask.service"
  }

  # password file
  provisioner "file" {
    content     = "user, password, host = \"${var.RDS_DATABASE_USER}\", \"${var.RDS_DATABASE_PASSWORD}\", \"${aws_db_instance.air_quality_database.address}\" "
    destination = "~/python-backend/variables.py"
  }

  # main api file
  provisioner "file" {
    source      = "../python-backend/main.py"
    destination = "~/python-backend/main.py"
  }

  # commands for provisioning
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo apt install -y python3 python3-pip",
      "sudo pip3 install flask_restful",
      "sudo pip3 install pandas",
      "sudo pip3 install mysql-connector-python",
      "chmod +x ~/python-backend",
      "sudo cp ~/python-backend/flask.service /etc/systemd/system/flask.service",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable flask.service",
      "sudo service flask start"
    ]
  }

  # private key pair needs to be added in the ci
  connection {
    type        = "ssh"
    user        = "ubuntu"
    password    = ""
    host        = aws_instance.ec2_backend.public_ip
    private_key = file("~/.ssh/id_rsa")
  }

}

resource "aws_key_pair" "deployer" {
  key_name   = "ec2-ssh-key"
  public_key = var.SSH_PUBLIC_KEY
}
