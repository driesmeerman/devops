resource "aws_cloudwatch_dashboard" "main" {
  dashboard_name = "air-polution-app-dashboard"

  dashboard_body = <<EOF
{
  "widgets": [
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/Lambda",
            "Invocations ",
            "FunctionName",
            "airQualityRetrieverHandler",
            {
              "yAxis": "right",
              "label": "Invocations",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Lambda monitor invocations",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/Lambda",
            "Duration",
            "FunctionName",
            "airQualityRetrieverHandler",
            {
              "yAxis": "right",
              "label": "Execution time",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Lambda monitor execution time",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/Lambda",
            "Errors",
            "FunctionName",
            "airQualityRetrieverHandler",
            {
              "yAxis": "right",
              "label": "Errors",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Lambda monitor errors",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/Lambda",
            "Throttles",
            "FunctionName",
            "airQualityRetrieverHandler",
            {
              "yAxis": "right",
              "label": "Throttles",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Lambda monitor throttle",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/EC2",
            "StatusCheckFailed",
            "InstanceId",
            "i-0386c96006d918649",
            {
              "yAxis": "right",
              "label": "StatusCheckFailed",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Backend monitor status checks",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/EC2",
            "CPUUtilization",
            "InstanceId",
            "i-0386c96006d918649",
            {
              "yAxis": "right",
              "label": "CPUUtilization",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Backend monitor cpu utilization",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/EC2",
            "NetworkIn",
            "InstanceId",
            "i-0386c96006d918649",
            {
              "yAxis": "right",
              "label": "NetworkIn",
              "period": 3600,
              "stat": "Maximum"
            }
          ],
          [
            "AWS/EC2",
            "NetworkOut",
            "InstanceId",
            "i-0386c96006d918649",
            {
              "yAxis": "right",
              "label": "NetworkOut",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "Backend monitor networking",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/RDS",
            "ReadLatency",
            "InstanceId",
            "terraform-20210317150735703800000001",
            {
              "yAxis": "right",
              "label": "ReadLatency",
              "period": 3600,
              "stat": "Maximum"
            }
          ],
          [
            "AWS/RDS",
            "WriteLatency",
            "InstanceId",
            "terraform-20210317150735703800000001",
            {
              "yAxis": "right",
              "label": "WriteLatency",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "RDS monitor latency",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/RDS",
            "CPUUtilization ",
            "InstanceId",
            "terraform-20210317150735703800000001",
            {
              "yAxis": "right",
              "label": "CPUUtilization",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "RDS monitor cpu utilization",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    },
    {
      "type": "metric",
      "x": 0,
      "y": 0,
      "width": 12,
      "height": 6,
      "properties": {
        "metrics": [
          [
            "AWS/RDS",
            "FreeStorageSpace",
            "InstanceId",
            "terraform-20210317150735703800000001",
            {
              "yAxis": "right",
              "label": "FreeStorageSpace",
              "period": 3600,
              "stat": "Maximum"
            }
          ]
        ],
        "period": 300,
        "stat": "Average",
        "region": "eu-west-1",
        "timezone": "+0100",
        "title": "RDS monitor storage space",
        "stacked": false,
        "view": "timeSeries",
        "liveData": false
      }
    }
  ]
}
EOF
}
