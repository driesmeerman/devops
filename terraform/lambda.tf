resource "aws_lambda_function" "air_retrieve_lamda" {
  filename      = "../lambda/air-retrieve/air-retrieve.zip"
  function_name = "airQualityRetrieverHandler"
  handler       = "src/handlers/air-quality-retriever.airQualityRetrieverHandler"
  runtime       = "nodejs14.x"
  timeout       = 100

  source_code_hash = filebase64sha256("../lambda/air-retrieve/air-retrieve.zip")

  role = aws_iam_role.iam_for_lambda.arn

  environment {
    variables = {
      RDS_DATABASE_HOST     = aws_db_instance.air_quality_database.address
      RDS_DATABASE_USER     = var.RDS_DATABASE_USER
      RDS_DATABASE_PASSWORD = var.RDS_DATABASE_PASSWORD
      airvisualApiKey       = var.AIR_VISUAL_KEY
    }
  }

  tags = {
    ManagedBy = "terraform"
    DevOps    = "air-retrieve-lambda"
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_cloudwatch_event_rule" "every_day" {
  name                = "every-day"
  description         = "Fires every day"
  schedule_expression = "rate(1 day)"

  tags = {
    ManagedBy = "terraform"
    DevOps    = "aws_cloudwatch_event_rule"
  }
}

resource "aws_cloudwatch_event_target" "retrieve_every_day" {
  rule      = aws_cloudwatch_event_rule.every_day.name
  target_id = "lambda"
  arn       = aws_lambda_function.air_retrieve_lamda.arn
  input     = jsonencode({ "locations" : [{ "country" : "Netherlands", "state" : "North Holland", "city" : "Amsterdam" }, { "country" : "Netherlands", "state" : "South Holland", "city" : "Rotterdam" }, { "country" : "USA", "state" : "California", "city" : "Los Angeles" }, { "country" : "USA", "state" : "New York", "city" : "New York City" }, { "country" : "United Kingdom", "state" : "England", "city" : "London" }, { "country" : "France", "state" : "Ile-de-France", "city" : "Paris" }, { "country" : "Germany", "state" : "Berlin", "city" : "Berlin" }, { "country" : "Sweden", "state" : "Stockholm", "city" : "Stockholm" }, { "country" : "China", "state" : "Beijing", "city" : "Beijing" }, { "country" : "Bangladesh", "state" : "Dhaka", "city" : "Dhaka" }] })
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.air_retrieve_lamda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.every_day.arn

}
