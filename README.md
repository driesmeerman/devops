# Devops climate change

Project to create more awareness around climate change and the environment.

# Terraform commands

- `$ terraform validate`
- `$ terraform plan`
- `$ terraform apply`
- `$ terraform destroy`
