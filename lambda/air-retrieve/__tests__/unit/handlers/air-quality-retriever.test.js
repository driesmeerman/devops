// Import all functions from hello-from-lambda.js
const lambda = require('../../../src/handlers/air-quality-retriever.js');

// This includes all tests for helloFromLambdaHandler()
describe('Test for air-quality-retriever', function () {
    // This test invokes helloFromLambdaHandler() and compare the result 
    it('Verifies successful response', async () => {
        const testEvent = {
            test: true,
            locations: [{
                country: "USA",
                state: "California",
                city: "Los Angeles"
            }]
        }

        const result = await lambda.airQualityRetrieverHandler(testEvent);

        const expectedResult = 'success';
        // Compare the result with the expected result
        expect(result).toEqual(expectedResult);
    });
});
