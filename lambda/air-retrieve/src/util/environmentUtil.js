function getApiKey() {
    return process.env.airvisualApiKey;
}

function getConnectionInfo() {
    const res =  {
        host: process.env.RDS_DATABASE_HOST,
        user: process.env.RDS_DATABASE_USER,
        password: process.env.RDS_DATABASE_PASSWORD,
        database: 'Devops'
    };

    return res;
}

exports.getApiKey = getApiKey;
exports.getConnectionInfo = getConnectionInfo;