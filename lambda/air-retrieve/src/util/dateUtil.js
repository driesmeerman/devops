function getTimeStamp(){
    const now = new Date();
    return now.toISOString();
}

exports.getTimeStamp = getTimeStamp;