const current = {};

function getLogger(level){
    if (!current.logger) {
        current.logger = new Logger(level);
    }

    return current.logger;
}

class Logger {
    constructor(level){
        if (level) {
            this.level = level;
        } else {
            this.level = LogLevel.error;
        }
        
    }

    info(...args){
        this.log(...args);
    }

    log(...args){
        if (this.level >= LogLevel.INFO)
            console.log(...args);
    }

    warn(...args){
        if (this.level >= LogLevel.WARN)
            console.warn(...args);
    }

    error(...args){
        if (this.level >= LogLevel.ERROR)
            console.error(...args);
    }


    setLevel(level){
        this.level = level;
    }

}

const LogLevel = {
    ALL: 20,
    INFO: 10,
    WARN: 5,
    ERROR: 2,
    NONE: 0
}

exports.Logger = Logger;
exports.LogLevel = LogLevel;
exports.getLogger = getLogger;