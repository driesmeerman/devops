const {createMeasurement} = require('../model/measurement')

class AirVisualServiceMock {
    async getLocationMeasurement(country, city, state){
        const mockResponse = {
            "status": "success",
            "data": {
                "city": "Los Angeles",
                "state": "California",
                "country": "USA",
                "location": {
                    "type": "Point",
                    "coordinates": [
                        -118.2417,
                        34.0669
                    ]
                },
                "current": {
                    "weather": {
                        "ts": "2021-03-10T08:00:00.000Z",
                        "tp": 9,
                        "pr": 1020,
                        "hu": 71,
                        "ws": 2.64,
                        "wd": 238,
                        "ic": "03n"
                    },
                    "pollution": {
                        "ts": "2021-03-10T08:00:00.000Z",
                        "aqius": 12,
                        "mainus": "p2",
                        "aqicn": 4,
                        "maincn": "p2"
                    }
                }
            }
        };

        return createMeasurement(city, mockResponse);
    }
}

exports.AirVisualServiceMock = AirVisualServiceMock;