const axios = require('axios');
const { Measurement } = require('../model/measurement');
const {createMeasurement} = require('../model/measurement')

const BASE_URL = 'http://api.airvisual.com/v2';

class AirVisualService {
    
    constructor (apiKey){
        this.key = apiKey;
    }

    /**
     * 
     * @param {string} country 
     * @param {string} state 
     * @param {string} city
     * @returns {Measurement}
     */
    async getLocationMeasurement(country, state, city) {
        const url = this.getUrl(country, state, city);
        const response = await axios.get(url);
        return createMeasurement(city, response.data);
    }

    getUrl(country, state, city){
        return `${BASE_URL}/city?city=${city}&state=${state}&country=${country}&key=${this.key}`
    }
}

exports.AirVisualService = AirVisualService