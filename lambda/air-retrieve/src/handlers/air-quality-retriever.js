const { AirVisualService } = require('../services/airVisualService');
const { AirVisualServiceMock } = require('../mock/airVisualServiceMock');
const { getAirQualityDB } = require('../db/database');
const { getApiKey } = require('../util/environmentUtil');
const { getLogger, LogLevel } = require('../util/logger');
const logger = getLogger(LogLevel.NONE);

/**
 * A Lambda function that returns a static string
 */
exports.airQualityRetrieverHandler = async (event) => {

    const apiKey = getApiKey();
    const locations = event.locations;
    const airService = event.test ? new AirVisualServiceMock() : new AirVisualService(apiKey);

    if (event.test) {
        logger.setLevel(LogLevel.NONE);
    } else if (!apiKey) {
        return 'No API key supplied in env vars, stopping execution';
    }

    if (!locations) {
        return 'No locations to sync, validate the event that was passed to this function.';
    }

    try {
        for (const location of locations) {
            await handleLocation(airService, location, !event.test);
            await wait(0.8);
        }
    } catch (error) {
        logger.info('Error', error.message);

        if (error.response) {
            logger.info('Message', error.response.data.message);
        }

        return 'error';
    }

    return 'success';
}

/**
 * 
 * @param {locationInput} location 
 */
async function handleLocation(airService, location, save) {
    const { country, city, state } = location;
    logger.log(`---- location ${city} ---- `);
    const measurement = await airService.getLocationMeasurement(country, state, city);
    logger.log(measurement.toSingleLineString());
    if (save) {
        const db = getAirQualityDB();
        const result = await db.addMeasurement(measurement);
    }

}

/**
 * wrapper to synchronously await a loop
 * @param {number} seconds 
 */
async function wait(seconds) {
    const millis = seconds * 1000;
    return new Promise((res) => {
        setTimeout(() => {
            res();
        }, millis)
    });
}

/**
 * @typedef locationInput
 * @property {string} country
 * @property {string} state
 * @property {string} city
 */