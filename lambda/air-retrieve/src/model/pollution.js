class Pollution {
    
    /**
     * 
     * @param {string} epa - air quality index based on US EPA standard
     * @param {string} mep - air quality index based China MEP standard
     */
    constructor(epa, mep){
        this.epa = epa;
        this.mep = mep;
    }
}

/**
 * parses current.pollution from a return from city data api
 * https://api-docs.iqair.com/?version=latest#5bc93d6b-d563-43dc-adb9-c266b2e96d4a
 * @param {*} data - JSON from api
 * @returns {Pollution}
 */
function pollutionfromAirVisualJSON(data){
    return new Pollution(data.aqius, data.aqius);
}

exports.Pollution = Pollution;
exports.pollutionfromAirVisualJSON = pollutionfromAirVisualJSON;