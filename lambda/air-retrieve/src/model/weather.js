class Weather {
    constructor(celcius, atmosphericPressure, hummidity, windSpeed, windDirection){
        this.celcius = celcius;
        this.atmosphericPressure = atmosphericPressure;
        this.hummidity = hummidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }
}

/**
 * see https://api-docs.iqair.com/?version=latest detailed response example
 * specifically forecasts comments
 * @param {*} data - api response
 */
function weatherFromAirVisualJSON(data){
    const {tp, pr, hu, ws, wd} = data;
    return new Weather(tp, pr, hu, ws, wd);
}

exports.Weather = Weather;
exports.weatherFromAirVisualJSON = weatherFromAirVisualJSON