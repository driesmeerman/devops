const {getTimeStamp} = require('../util/dateUtil');
const {pollutionfromAirVisualJSON} = require('./pollution');
const {weatherFromAirVisualJSON} = require('./weather');


class Measurement {
    constructor(city, weather, pollution){
        this.city = city;
        this.weather = weather;
        this.pollution = pollution;
        this.timestamp = getTimeStamp();
    }

    /**
     * returns a single line string, for usage in aws cli
     */
    toSingleLineString(){
        return `${this.city} - epa: ${this.pollution.epa} ; mep: ${this.pollution.mep} ; temp: ${this.weather.celcius} ; pressure: ${this.weather.atmosphericPressure} ; wind: speed: ${this.weather.windSpeed} direction: ${this.weather.windDirection}`;
    }
}

function createMeasurement(city, response){
    const pollution = pollutionfromAirVisualJSON(response.data.current.pollution);
    const weather = weatherFromAirVisualJSON(response.data.current.weather);
    return new Measurement(city, weather, pollution);
}

exports.Measurement = Measurement;
exports.createMeasurement = createMeasurement;