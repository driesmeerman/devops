const {getConnectionInfo, getApiKey} = require('../util/environmentUtil');
const connectionInfo = getConnectionInfo();

const mysql = require('mysql');
const { Measurement } = require('../model/measurement');
const { Weather } = require('../model/weather');


const current = {};

class AirQualityDB {
    constructor(){
        this.connection = mysql.createConnection(connectionInfo);
    }

    /**
     * 
     * @param {Measurement} measurement 
     */
    async addMeasurement(measurement){
        const query = `INSERT INTO Measurement SET ?`;
        const measure = {
            city: measurement.city,
            time_stamp: measurement.timestamp
        };

        const measureInsert = await this.execute(query, measure);
        const measurementId = measureInsert.insertId;
        const pollutionInsert = await this.addPollution(measurementId, measurement.pollution);
        const weatherInsert = await this.addWeather(measurementId, measurement.weather);
    }

    async addPollution(measurementId, pollution){
        const query = `INSERT INTO Pollution SET ?`;
        const pol = {
            measurement_id: measurementId,
            epa_air_quality_index: pollution.epa,
            mep_air_quality_index: pollution.mep
        }

        return await this.execute(query, pol);
    }

    /**
     * 
     * @param {string} measurementId 
     * @param {Weather} weather 
     * @returns 
     */
    async addWeather(measurementId, weather){
        const query = `INSERT INTO Weather SET ?`;
        const pol = {
            measurement_id: measurementId,
            temperature_celsius: weather.celcius,
            min_temperature_celsius: weather.min_celcius,
            atmospheric_pressure_hpa: weather.atmosphericPressure,
            humidity: weather.hummidity,
            wind_speed: weather.windSpeed ,
            wind_direction: weather.windDirection
        }

        return await this.execute(query, pol);
    }


    async execute(query, data){
        return new Promise((resolve, reject) => {
            this.connection.query(query, data, (err, result) => {
                if (err){
                    reject(err);
                }
                resolve(result);
            });
        });
    }

}

/**
 * 
 * @returns AirQualityDB
 */
function getAirQualityDB (){
    if (!current.db) {
        current.db = new AirQualityDB();
    }

    return current.db;
}

exports.getAirQualityDB = getAirQualityDB;