import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Credits from '@/views/Credits.vue'
import Profile from "@/views/Profile";
import Search from "@/views/Search";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/search',
    name: 'Search',
    // route level code-splitting
    // this generates a separate chunk (search.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Search// () => import(/* webpackChunkName: "search" */ '../views/Search.vue')
  },
  {
    path: '/credits',
    name: 'Credits',
    component: Credits
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
