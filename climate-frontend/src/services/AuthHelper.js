import moment from "moment";

export function saveAuth(data) {
    const expires = moment().add(data.expires_in, 'seconds');
    const tokens = {
        access: data.access_token,
        id: data.id_token,
        refresh: data.refresh_token,
        expires_in: data.expires_in,
        expires: expires.format()
    }
    localStorage.setItem('auth_tokens', JSON.stringify(tokens))
}

export function getAuth() {
    const data = localStorage.getItem('auth_tokens');
    if (!data) {
        return null;
    }

    const auth = JSON.parse(data);
    auth.expires = moment(auth.expires);
    if (auth.expires.isBefore(moment())) {
        return null;
    }

    auth.isExpired = () => {
        return auth.expires.isBefore(moment());
    }

    return auth;


}