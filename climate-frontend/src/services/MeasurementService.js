import {getAuth} from './AuthHelper';
import axios from 'axios';
import {CONSTANTS} from '../constants';

const BASE_URL = CONSTANTS.BACKEND_BASE_URL;

export class MeasurementService{
    constructor(auth) {
        if (auth) {
            this.auth = auth;
        } else {
            this.auth = getAuth();
        }

        if (!this.auth) {
            throw Error('Invalid authentication')
        }

        const instance = axios.create();
        instance.interceptors.request.use((config) => {
            config.headers['Authorization'] = `Bearer ${this.auth.id}`
            return config;
        });
        this.axios = instance;
    }

    async getMeasurement(city, date){
        const url = `${BASE_URL}/climate/measurement?city=${city}&date=${date}`;
        try {
            const response = await this.axios.get(url);
            if (Array.isArray(response.data)){
                return response.data.map(transformDataToMeasurement);
            }

            return null;

        } catch (e) {
            return null;
        }
    }

    async getPopularSearches() {
        const url = `${BASE_URL}/climate/popular`;
        try {

            const response = await this.axios.get(url);
            if (Array.isArray(response.data)){
                return response.data;
            }

            return null;

        } catch (e) {
            return null;
        }
    }
}

function transformDataToMeasurement(data){
    return {
        timestamp: data.time_stamp,
        pollution: {
            epa: data.epa_air_quality_index,
            mep: data.mep_air_quality_index,
        },
        weather: {
            temperature: data.temperature_celsius,
            pressure: data.atmospheric_pressure_hpa,
            humidity: data.humidity,
            wind_speed: data.wind_speed,
            wind_direction: data.wind_direction
        }
    }
}