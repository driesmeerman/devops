import {getAuth} from './AuthHelper';
import axios from 'axios';
import { CONSTANTS } from '../constants';

const cognitoHost = CONSTANTS.COGNITO_URL;
// const clientId = "6s70rr3molijo1649tfuuupga2";
// const redirectUri = "http://localhost:8080/login";
const authUrl = `${cognitoHost}/oauth2/userInfo`;

export class IncognitoService {
    constructor(auth) {
        if (auth) {
            this.auth = auth;
        } else {
            this.auth = getAuth();
        }

        if (!this.auth) {
            throw Error('Invalid authentication')
        }
    }

    async getProfileData(){
        try {
            const config = {
                headers: { Authorization: `Bearer ${this.auth.access}` }
            };
            const response = await axios.get(authUrl, config);
            console.log(response.data);

            return response.data;
        } catch (e) {
            return null;
        }
    }
}