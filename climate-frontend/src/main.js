import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BootstrapVue } from 'bootstrap-vue';

Vue.config.productionTip = false
Vue.use(BootstrapVue);


const store = {
  state: {
    loginToken: null
  }
};

new Vue({
  data: {
    sharedState: store.state
  },
  router,
  render: h => h(App)
}).$mount('#app')
