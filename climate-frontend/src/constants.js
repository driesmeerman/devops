export const CONSTANTS = {
    BACKEND_BASE_URL: "https://devops.chrozera.xyz/",
    COGNITO_URL: "https://devops-team-10-uva-2021.auth.eu-west-1.amazoncognito.com"
}