variables:
  TF_ROOT_DIR: ${CI_PROJECT_DIR}/terraform
  DATABASE_ROOT_DIR: ${CI_PROJECT_DIR}/database
  EC2_ROOT_DIR: ${CI_PROJECT_DIR}/python-backend
  LAMDA_ROOT_DIR: ${CI_PROJECT_DIR}/lambda/air-retrieve
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/default
  TF_VAR_AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  TF_VAR_AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  TF_VAR_AWS_SESSION_TOKEN: $AWS_SESSION_TOKEN
  TF_VAR_RDS_DATABASE_USER: $RDS_DATABASE_USER
  TF_VAR_RDS_DATABASE_PASSWORD: $RDS_DATABASE_PASSWORD
  TF_VAR_RDS_DATABASE_NAME: $RDS_DATABASE_NAME
  TF_VAR_SSH_PUBLIC_KEY: $SSH_PUBLIC_KEY
  TF_VAR_AIR_VISUAL_KEY: $AIR_VISUAL_KEY

stages:
  - lint
  - build
  - test
  - deploy-infrastructure
  - deploy-code
  - cleanup

##############################################################################
# Lint jobs
##############################################################################
vue-lint:
  image:
    name: node:current-alpine
  stage: lint
  allow_failure: true
  before_script:
    - cd climate-frontend
    - npm ci
  script: npm run lint

##############################################################################
# Build jobs
##############################################################################

vue-build:
  image:
    name: node:current-alpine
  stage: build
  before_script:
    - cd climate-frontend
    - npm install
  script:
    - npm run build
  artifacts:
    name: climate_frontend_$CI_COMMIT_TIMESTAMP
    paths:
      - climate-frontend/dist/

lambda-build:
  image:
    name: python:3.8
  stage: build
  before_script:
    - cd ${LAMDA_ROOT_DIR}
    - curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
    - apt-get install -y nodejs
    - apt-get install -y zip
    - pip3 install awscli --upgrade
    - pip3 install aws-sam-cli --upgrade
  script:
    - sam build
    - cd .aws-sam/build/airQualityRetrieverHandler/
    - zip -r air-retrieve.zip ./
    - mv air-retrieve.zip ${LAMDA_ROOT_DIR}/air-retrieve.zip
  artifacts:
    name: air-retrieve-build
    paths:
      - ${LAMDA_ROOT_DIR}/air-retrieve.zip

##############################################################################
# Test jobs
##############################################################################

tf-prepare:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT_DIR}
    - mkdir ~/.ssh && touch ~/.ssh/id_rsa && echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa && chmod 700 ~/.ssh
    - gitlab-terraform init
  stage: test
  script:
    - gitlab-terraform validate

lambda-air-retrieve-test:
  image:
    name: node:current-alpine
  stage: test
  before_script:
    - cd ${LAMDA_ROOT_DIR}
    - npm ci
  script: npm run test

##############################################################################
# Infrastructure jobs
##############################################################################

tf-deploy:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT_DIR}
    - apk add mysql mysql-client openssh-client bash
    - mkdir ~/.ssh && touch ~/.ssh/id_rsa && echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa && chmod 700 ~/.ssh
    - gitlab-terraform init
  stage: deploy-infrastructure
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
    - gitlab-terraform apply
  artifacts:
    reports:
      terraform: ${TF_ROOT_DIR}/plan.json
  when: manual
  only:
    - main

##############################################################################
# Deploy jobs
##############################################################################

vue-deploy:
  image:
    name: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  stage: deploy-code
  # Make sure that the first path is the same as artifact path
  # Make sure that the bucket name is the same as in main.tf file TODO:discuss with team
  script: aws s3 cp climate-frontend/dist/ s3://frontend-root-devops-team/ --recursive --include "*"
  when: manual
  only:
    - main

ec2-deploy:
  before_script:
    - "command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )"
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config'
  stage: deploy-code
  script:
    - ssh ubuntu@$EC2_ADDRESS 'sudo service flask stop'
    - scp -r $EC2_ROOT_DIR ubuntu@$EC2_ADDRESS:/home/ubuntu
    - ssh ubuntu@$EC2_ADDRESS 'sudo service flask start'
  when: manual

##############################################################################
# Cleanup jobs
##############################################################################

tf-destroy:
  image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
  before_script:
    - cd ${TF_ROOT_DIR}
    - gitlab-terraform init
  stage: cleanup
  script:
    - gitlab-terraform destroy
  when: manual
  only:
    - main
