# Architecture
This folder contains architecture documentation.
Architecture.xml can be loaded into draw.io
## Overall
![Overal architecture](architecture-overview.png)

## Database
![Database](architecture-DB.png)