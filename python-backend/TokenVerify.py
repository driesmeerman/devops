# Copyright 2017-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.

import json
import time
import urllib.request
from jose import jwk, jwt
from jose.utils import base64url_decode

region = 'eu-west-1'
userpool_id = 'eu-west-1_Rq2OVdSuK'
app_client_id = '6s70rr3molijo1649tfuuupga2'
keys_url = 'https://cognito-idp.{}.amazonaws.com/{}/.well-known/jwks.json'.format(region, userpool_id)
# instead of re-downloading the public keys every time
# we download them only on cold start
# https://aws.amazon.com/blogs/compute/container-reuse-in-lambda/
with urllib.request.urlopen(keys_url) as f:
  response = f.read()
keys = json.loads(response.decode('utf-8'))['keys']

def lambda_handler(event, context):
    token = event['token']
    print(token)
    # get the kid from the headers prior to verification
    headers = jwt.get_unverified_headers(token)
    kid = headers['kid']
    # search for the kid in the downloaded public keys
    key_index = -1
    for i in range(len(keys)):
        if kid == keys[i]['kid']:
            key_index = i
            break
    if key_index == -1:
        print('Public key not found in jwks.json')
        return False
    # construct the public key
    public_key = jwk.construct(keys[key_index])
    # get the last two sections of the token,
    # message and signature (encoded in base64)
    message, encoded_signature = str(token).rsplit('.', 1)
    # decode the signature
    decoded_signature = base64url_decode(encoded_signature.encode('utf-8'))
    # verify the signature
    if not public_key.verify(message.encode("utf8"), decoded_signature):
        print('Signature verification failed')
        return False
    print('Signature successfully verified')
    # since we passed the verification, we can now safely
    # use the unverified claims
    claims = jwt.get_unverified_claims(token)
    # additionally we can verify the token expiration
    if time.time() > claims['exp']:
        print('Token is expired')
        return False
    # and the Audience  (use claims['client_id'] if verifying an access token)
    if claims['aud'] != app_client_id:
        print('Token was not issued for this audience')
        return False
    # now we can use the claims
    print(claims)
    return claims

# the following is useful to make this script executable in both
# AWS Lambda and any other local environments
if __name__ == '__main__':
    # for testing locally you can enter the JWT ID Token here
    event = {'token': 'eyJraWQiOiJTZUk1Qm1MbGZDczBSMzBEaTJkU0NwSTl3ZndUOUs4V29ldnd3UXZoSlNRPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoiY3I2aGU1WElRTy02SlB6M2pJeW9qUSIsInN1YiI6ImE2ZWI0MzMzLWJiYmItNGJmMy04NDVjLWViODQyZWVjM2U0YyIsImF1ZCI6IjZzNzBycjNtb2xpam8xNjQ5dGZ1dXVwZ2EyIiwiZXZlbnRfaWQiOiIwYjlkZDgxNi1mOWY0LTRhODAtYTc5MC1lOGU0MmI4YTkxNjkiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYxNjE0NjA2MSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LXdlc3QtMS5hbWF6b25hd3MuY29tXC9ldS13ZXN0LTFfUnEyT1ZkU3VLIiwiY29nbml0bzp1c2VybmFtZSI6ImE2ZWI0MzMzLWJiYmItNGJmMy04NDVjLWViODQyZWVjM2U0YyIsImV4cCI6MTYxNjE0OTY2MSwiaWF0IjoxNjE2MTQ2MDYxLCJlbWFpbCI6InJlaWpuZGVyc2Jqb3JuQGdtYWlsLmNvbSJ9.FQ6LKKx98jBIvysLfSI6NaElnNmUmlOl4SZp-uEkVEEyje5Stm2ZmADfyJKeEeZEqjy8aVJOsA04qbKCDPJDLf5kvEnpLMII4UGbw9MHuAQ85ruPGQMHGqARpb5bspKHtTfz0ATL4RZVusyAT-h-Wdj6icr7kCEg0oMGqDes-XNoDrS3empHfd6JrjWs2OkIT6IErPnOxi5YLQ94v9q5x3Ib9wCxZzGK0j0hNNs9abjn-vc6c0s5cfUkB7vZftxmo4PcrvOBLUAI-oV3Uxw48qBw90p6eWaTn54Dh81A5fEOz013nwvLH-dKLn4nX_g81k2CqXWnFUPeC4lNR8zv9g'}
    lambda_handler(event, None)

    # eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzb21lIjoicGF5bG9hZCJ9.4twFt5NiznN84AWoo1d7KO1T_yoc0Z6XOpOVswacPZg
