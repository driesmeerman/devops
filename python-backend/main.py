#!/usr/bin/env python3

from flask import Flask, request
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast
import mysql.connector
import variables
import TokenVerify
import dateutil.parser
from datetime import timedelta, datetime
from flask_cors import CORS

# pip3 install python-jose
# pip3 install flask_cors

app = Flask(__name__)
CORS(app)
api = Api(app, "/climate")

class Measurement(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('city', required=True)
        parser.add_argument('date', required=False)
        args = parser.parse_args()


        if 'Authorization' not in request.headers:
            return "access denied", 403

        if request.headers['Authorization'] == '':
            return "access denied", 403

        event = {'token': request.headers['Authorization'][7:]}
        getacces = TokenVerify.lambda_handler(event, None)
        if getacces is False:
            return "access denied", 401

        if args['date'] is not None:
            lastdate = args['date']

        else:
            lastdate = datetime.now() - timedelta(1)
            lastdate = lastdate.isoformat()

        firstdate = dateutil.parser.parse(lastdate) - timedelta(6)
        firstdate = firstdate.isoformat()

        cnx = mysql.connector.connect(user=variables.user,
                                      password=variables.password,
                                      host=variables.host,
                                      database="Devops")

        try:
           cursor = cnx.cursor()

           select_stmt = ("SELECT * FROM Measurement INNER JOIN Pollution ON"
           " Measurement.id=Pollution.measurement_id INNER JOIN Weather ON "
           "Weather.measurement_id=Pollution.measurement_id WHERE city = '" +
           args['city'] +"' AND time_stamp <= '" + lastdate +
           "' AND time_stamp >= '" + firstdate + "' ;")

           cursor.execute(select_stmt)
           rows = cursor.fetchall()

           columns = [desc[0] for desc in cursor.description]
           result = []

           for row in rows:
               row = list(row)
               row[2] = row[2].isoformat()
               row = tuple(row)
               row = dict(zip(columns, row))
               result.append(row)

        finally:
            cnx.close()

        return result, 200  # return data and 200 OK code

class Popular(Resource):
    def get(self):

        if 'Authorization' not in request.headers:
            return "access denied", 403

        if request.headers['Authorization'] == '':
            return "access denied", 403
                    
        event = {'token': request.headers['Authorization'][7:]}
        getacces = TokenVerify.lambda_handler(event, None)
        if getacces is False:
            return "access denied", 401

        popular = ["Amsterdam", "Brussel", "Rotterdam"]
        return popular , 200  # return data and 200 OK code

api.add_resource(Measurement, '/measurement')  # '/users' is our entry point for Users
api.add_resource(Popular, '/popular')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
