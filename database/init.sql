

CREATE DATABASE IF NOT EXISTS Devops;
USE Devops;

CREATE TABLE IF NOT EXISTS Measurement (
    id INT AUTO_INCREMENT NOT NULL UNIQUE,

    city VARCHAR(255) NOT NULL,
    time_stamp TIMESTAMP NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Weather (
    id INT AUTO_INCREMENT NOT NULL UNIQUE,
    measurement_id INT NOT NULL UNIQUE,

    temperature_celsius DOUBLE,
    min_temperature_celsius DOUBLE,
    atmospheric_pressure_hpa DOUBLE,
    humidity DOUBLE,
    wind_speed DOUBLE,
    wind_direction INT,

    PRIMARY KEY (id),
    FOREIGN KEY (measurement_id) REFERENCES Measurement (id)
);


CREATE TABLE IF NOT EXISTS Pollution (
    id INT AUTO_INCREMENT NOT NULL UNIQUE,
    measurement_id INT NOT NULL UNIQUE,

    epa_air_quality_index INT,
    mep_air_quality_index INT,

    PRIMARY KEY (id),
    FOREIGN KEY (measurement_id) REFERENCES Measurement (id)
);

